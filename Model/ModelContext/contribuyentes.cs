namespace Model.ModelContext
{
    using Model.ModelContext;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Data.Sql;
    using System.Data.SqlClient;

    public partial class contribuyentes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public contribuyentes()
        {
            negocio = new HashSet<negocio>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string nombre_c { get; set; }

        [Required]
        [StringLength(50)]
        public string apellido_c { get; set; }

        
        [StringLength(50)]
        public string cedula_c { get; set; }

        [Required]
        [StringLength(50)]
        public string direccion_c { get; set; }

        [Required]
        [StringLength(50)]
        public string fecha_ing_c { get; set; }

        
        [StringLength(50)]
        public string telefono { get; set; }

        
        [StringLength(50)]
        public string email { get; set; }


       

       

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<negocio> negocio { get; set; }


        public List<contribuyentes> Listar()
        {
            var cont = new List<contribuyentes>();
            try
            {
                using (var context = new ImiContext())
                {
                    cont = context.contribuyentes.ToList();
                    var con = context.AspNetUserRoles.Where(x => x.RoleId == "2a4e987a-3cdb-4f33-a358-81c38ec002d6")
                                                     .Select(x => x.UserId)
                                                     .ToList();

                    //cont = context.contribuyentes.Where(x => con.Contains(x.id)).ToList();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return cont;

        }


        public contribuyentes Obtener(int id)
        {
            var cont = new contribuyentes();
          try
            {
                using (var ctx = new ImiContext())
                {
                    cont = ctx.contribuyentes.Include("negocio")
                                            .Where(x => x.id == id)
                                            .SingleOrDefault();
                }
            }
            catch (ValidationException e)
            {

                throw new Exception(e.Message);
            }
            return cont;
        }


        public List<contribuyentes> Todo()
        {
            var mes = new List<contribuyentes>();
            try
            {
                using (var context = new ImiContext())
                {
                    mes = context.contribuyentes.ToList();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return mes;

        }


        public void Guardar(string cedula)
        {
            try
            {

                using (var ctx = new ImiContext())
                {

                    //ctx.Database.ExecuteSqlCommand(
                    //    "select * from contribuyentes where cedula_c=@cedula",
                    //    new SqlParameter("cedula", this.cedula_c)
                    // );
                    if (this.id==0)
                    {
                        ctx.Entry(this).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        ctx.Entry(this).State = System.Data.Entity.EntityState.Modified;
                    }
                    ctx.SaveChanges();

                }


            }
            catch (SqlException e)
            {

                throw new Exception(e.Message);
            }
        }


    }
}
