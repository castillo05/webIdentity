﻿using Model;
using Model.ModelContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IdentiyApp.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        //private AspNetUsers AspNetUser_ = new AspNetUsers();
        private contribuyentes cont = new contribuyentes();

        public ActionResult Index(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return View(cont.Listar());
            }
            else
            {
                ViewBag.Name = name;
                return View(cont.Listar().Where(c => c.nombre_c.ToLower().Contains(name)));
            }



        }


    

    public JsonResult LlamarJson(string name)
    {
        var output = cont.Listar();
        return Json(output, JsonRequestBehavior.AllowGet);
    }
    public ActionResult About()
        {
            ViewBag.Message = "Pago Realizado Con Exito...";

            return View();
        }
        
        public ActionResult Contact(int Id = 0)
        {
            ViewBag.Message = "Your contact page.";

            return View(
                Id > 0 ? cont.Obtener(Id)
                : cont
                );
        }

        public ActionResult Guardar(contribuyentes model,string cedula)
        {
            model.Guardar(cedula);
            return Redirect("~/Home/Index/");
        }
    }
}